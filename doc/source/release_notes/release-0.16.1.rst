***************
Release  0.16.1
***************

* Fix issue with scm logic sometimes trying to re-clone an extern repository
* Fix issue with ScmGit applying patches on updates
* Add missing SdkSource to the environment.
* Address issue with CCopy when coping a directory that contains a lot of dead symlinks.
* Fix regression with warning messages reporting as error messages