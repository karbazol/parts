.. Parts documentation master file, created by
   sphinx-quickstart on Tue Aug 20 15:51:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Parts's documentation!
=================================

Parts is a build extension to the SCons build system.

.. note::

   This documentation is being re-written from the old word doc.
   It is current in a work in progress state as it is being updated.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:
   
   release_notes/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
