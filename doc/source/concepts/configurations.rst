Configurations
--------------

Parts adds the build configuration which are a set of settings to apply for a given
tool based on the system being built for.
Common configurations are 'debug' and 'release'. 
Users can define their own named configurations based on existing configurations,
allowing for various modification to preexisting settings or override existing configuration
with desired values.
These can be installed independent of the Parts install via the use of 
the parts-site directory